import os

import requests
from rest_framework.decorators import api_view
from rest_framework.response import Response


def get_api_url():
    return f"{os.environ.get('API_HOST')}"


def get_api_headers():
    return {
        "X-API-Key": os.environ.get("API_KEY"),
        "Content-Type": "application/x-www-form-urlencoded",
    }


@api_view(["get"])
def get_providers(request, format=None):
    res = requests.get(
        f"{get_api_url()}/provider/",
        headers=get_api_headers(),
    )
    return Response(data=res.json(), status=res.status_code)


@api_view(["POST"])
def login(request, format=None):
    res = requests.post(
        f"{get_api_url()}/login/",
        data=request.data,
        headers=get_api_headers(),
    )
    return Response(data=res.json(), status=res.status_code)


@api_view(["GET"])
def get_accounts(request, format=None):
    key = request.headers.get("x-key")
    if not key:
        return Response({"status": "error", "message": "invalid_key"})

    res = requests.get(
        f"{get_api_url()}/account/",
        params={
            "key": key,
        },
        headers=get_api_headers(),
    )
    return Response(data=res.json(), status=res.status_code)
