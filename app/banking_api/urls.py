from django.urls import path

from .views import get_accounts, get_providers, login

urlpatterns = [
    path("login/", login),
    path("accounts/", get_accounts),
    path("providers/", get_providers),
]
