from django.urls import include, path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.documentation import include_docs_urls

from .views import ping

schema_view = get_schema_view(
    openapi.Info(
        title="Prometeo Challenge API",
        default_version="v1",
    ),
    public=True,
    permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path("ping/", ping, name="ping"),
    path(
        "swagger-docs/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("docs/", include_docs_urls(title="Promete Challenge API")),
    path("api/v1/", include("banking_api.urls")),
]
